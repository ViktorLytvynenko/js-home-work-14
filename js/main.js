let inputElement = document.getElementById("input");
let bodyElement = document.body;

if (localStorage.getItem("theme") === "dark") {
    inputElement.checked = true;
    bodyElement.classList.add("dark");
}

inputElement.addEventListener("change", () => {
    if (inputElement.checked) {
        bodyElement.classList.add("dark");
        localStorage.setItem("theme", "dark");
    } else {
        bodyElement.classList.remove("dark");
        localStorage.setItem("theme", "light");
    }
});
